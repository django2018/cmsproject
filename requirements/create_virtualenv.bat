rem WORKSPACE - where to store the projects
rem set WORKSPACE=D:\NGUYEN_DUC_TAM\working
rem VIRTUALENVFOLDER - where to create the virtualenv
set VIRTUALENVFOLDER=C:\virtualenv\python37
set PATH=C:\python370;C:\python370\Scripts;%PATH%
set PYTHONPATH=C:\python370\Lib\site-packages;%PYTHONPATH%
virtualenv --python=python %VIRTUALENVFOLDER%
start 