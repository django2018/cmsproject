from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^category/(?P<slug>[-\w]+)/$', views.category, name='cms-category'),
    url(r'^search/$', views.search, name='cms-search'),
]

urlpatterns += [
    url(r'^$', views.StoryListView.as_view(), name='cms-home'),
    url(r'^(?P<slug>[-\w]+)/$', views.StoryDetailView.as_view(), name='cms-story'),
]